An LTS security update is needed for the following source packages.

To add a new entry, please coordinate with this week's Front-Desk
person, and use the 'package-operations' LTS tool.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

When checking what packages to work on, use:
$ ./find-work
from the LTS admin repository, to sort packages by priority and
display important notes about the package (special attention, VCS,
testing procedures, programming language, etc.).

To work on a package, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

To make it easier to see the entire history of an update, please append notes
rather than remove/replace existing ones.

--
ansible (rouca)
  NOTE: 20231202: Added by Front-Desk (Beuc)
  NOTE: 20231202: Supported package, but there's a CVE backlog, and no updates since 2021
  NOTE: 20231202: (neither in LTS nor in stable/oldstable), so this is an opportunity to
  NOTE: 20231202: assess/fix the situation.
  NOTE: 20231217: Begin to triage CVEs (rouca)
  NOTE: 20231217: Triaging done a few mail send upstream for claryfication purposes (rouca)
--
asterisk
  NOTE: 20231210: Added by Front-Desk (ta)
--
bind9 (Thorsten Alteholz)
  NOTE: 20230921: Added by Front-Desk (apo)
  NOTE: 20231008: backporting patches
  NOTE: 20231217: almost done with testing
--
bouncycastle (Markus Koschany)
  NOTE: 20231127: Added by Front-Desk (Beuc)
  NOTE: 20231127: Also fix pending no-dsa CVEs, in particular CVE-2020-26939 was fixed in stretch-lts (Beuc/front-desk)
  NOTE: 20231128: I can't find changes in PEMParser.java related to CVE-2023-33202, maybe contact upstream (Beuc/front-desk)
  NOTE: 20231218: Decision impending. (apo)
--
cacti (Sylvain Beucler)
  NOTE: 20230906: Added by Front-Desk (lamby)
  NOTE: 20231205: Triaging CVEs backlog (Beuc)
  NOTE: 20231218: Keep triaging CVEs backlog (Beuc)
--
cairosvg
  NOTE: 20230323: Added by Front-Desk (gladk)
  NOTE: 20230411: Proposed solution for CVE-2023-27586 in Buster to backport the --unsafe switch, introduced in 1.0.21, might work (dleidert/inactive)
--
cinder
  NOTE: 20230525: Added by Front-Desk (lamby)
  NOTE: 20230525: NB. CVE-2023-2088 filed against python-glance-store, python-os-brick, nova and cinder.
--
docker.io
  NOTE: 20230303: Added by Front-Desk (Beuc)
  NOTE: 20230303: Follow fixes from bullseye 11.2 (3 CVEs) (Beuc/front-desk)
  NOTE: 20230424: Is in preparation. (gladk)
  NOTE: 20230706: ask for review testing https://lists.debian.org/debian-lts/2023/07/msg00013.html
  NOTE: 20230801: rouca and santiago testing the swarm overlay network (including current buster version)
--
dogecoin
  NOTE: 20230619: Added by Front-Desk (Beuc)
  NOTE: 20230619: CVE-2021-37491 and CVE-2023-30769 seem forgotten by upstream,
  NOTE: 20230619: I suggest pinging/coordinating with upstream to know the current status;
  NOTE: 20230619: also I just referenced 3 older bitcoin-related CVEs to fix;
  NOTE: 20230619: dogecoin not present in bullseye/bookworm, so we lead the initiatives. (Beuc/front-desk)
--
dropbear (guilhem)
  NOTE: 20231219: Added by Front-Desk (ta)
--
frr
  NOTE: 20231119: Added by Front-Desk (apo)
--
golang-go.crypto
  NOTE: 20231219: Added by Front-Desk (ta)
--
haproxy
  NOTE: 20231217: Added by Front-Desk (utkarsh)
--
i2p
  NOTE: 20230809: Added by Front-Desk (Beuc)
  NOTE: 20230809: Experimental issue-based workflow: please self-assign and follow https://salsa.debian.org/lts-team/lts-updates-tasks/-/issues/28
--
imagemagick
  NOTE: 20230622: Added by Front-Desk (Beuc)
  NOTE: 20230622: Requested by maintainer (rouca) to tidy remaining open CVEs (Beuc/front-desk)
  NOTE: 20231014: Some work under git branch debian/buster but unease
--
keystone
  NOTE: 20231102: Added by Front-Desk (lamby)
  NOTE: 20231102: Sync (eg. CVE-2021-38155) with stable etc. (lamby)
--
knot-resolver
  NOTE: 20231029: Added by Front-Desk (gladk)
--
libreoffice
  NOTE: 20231217: Added by Front-Desk (utkarsh)
--
libreswan
  NOTE: 20230817: Added by Front-Desk (ta)
  NOTE: 20230909: Prepared a patch for CVE-2023-38712 and pushed it to
  NOTE: 20230909: https://salsa.debian.org/lts-team/packages/libreswan.git on the experimental
  NOTE: 20230909: branch. Upstream patch for CVE-2023-38710 does not apply at
  NOTE: 20230909: all due to code refactoring. I intend to package the version
  NOTE: 20230909: from Bullseye instead as soon as the maintainer uploads the fix. (apo)
--
libssh
  NOTE: 20231219: Added by Front-Desk (ta)
--
libstb
  NOTE: 20231029: Added by Front-Desk (gladk)
  NOTE: 20231029: A lot of open CVEs. Maybe duplicates.
  NOTE: 20231029: If you take a package, please evaluate it as well as its importance.
  NOTE: 20221119: None of the new CVE fixes has been reviewed by upstream so far,
  NOTE: 20221119: and in the past CVE fixes have caused regressions.
  NOTE: 20221119: Wait for upstream merge of fixes (and fixing in unstable). (bunk)
--
linux (Ben Hutchings)
  NOTE: 20230111: perma-added for LTS package-specific delegation (bwh)
--
linux-5.10
  NOTE: 20231005: perma-added for LTS package-specific delegation (bwh)
--
mariadb-10.3
  NOTE: 20231129: Added by Front-Desk (Beuc)
--
netatalk (Abhijith PA)
  NOTE: 20231119: Added by Front-Desk (apo)
--
node-webpack
  NOTE: 20231005: Added by Front-Desk (Beuc)
  NOTE: 20231005: Follow fixes from bullseye 11.7 (1 CVE) (Beuc/front-desk)
--
nova
  NOTE: 20230302: Re-add, request by maintainer (Beuc)
  NOTE: 20230302: zigo says that DLA 3302-1 ships a buster-specific CVE-2022-47951 backport that introduces regression
  NOTE: 20230302: (it's meant to check whether a VMDK image has the "monoliticFlat" subtype, but in practice it breaks compute nodes);
  NOTE: 20230302: cf. debian/patches/cve-2022-47951-nova-stable-rocky.patch, which depends on images_*.patch.
  NOTE: 20230302: "The upstream patch introduces a whitelist of allowed subtype (with monoliticFlat disabled by default).
  NOTE: 20230302:  Though in the Buster codebase, there was no infrastructure to check for this subtype ..." (zigo)
  NOTE: 20230302: Later suites (e.g. bullseye) ship a direct upstream patch and are not affected.
  NOTE: 20230302: We can either rework the patch, or disable .vmdk support entirely.
  NOTE: 20230302: zigo currently has no time and requests the LTS team to do it (IRC #debian-lts 2023-03-02). (Beuc/front-desk)
  NOTE: 20230525: NB. CVE-2023-2088 filed against python-glance-store, python-os-brick, nova and cinder. (lamby)
--
nvidia-cuda-toolkit
  NOTE: 20230514: Added by Front-Desk (utkarsh)
  NOTE: 20230514: package listed in packages-to-support; a bunch of CVEs have
  NOTE: 20230514: piled up. (utkarsh)
  NOTE: 20230610: Details: https://lists.debian.org/debian-lts/2023/06/msg00032.html
  NOTE: 20230610: my recommendation would be to put the package on the "not-supported" list. (tobi)
--
openssh
  NOTE: 20231219: Added by Front-Desk (ta)
--
osslsigncode
  NOTE: 20230925: Added by Front-Desk (apo)
  NOTE: 20230925: Maybe a new upstream release should just do the trick here.
--
python-django (Chris Lamb)
  NOTE: 20231006: Added by Front-Desk (Beuc)
  NOTE: 20231006: Fix the 4 no-dsa issues that are fixed in all other dists (Beuc/front-desk)
  NOTE: 20231020: ^ CVE-2021-28658, CVE-2021-31542, CVE-2021-33203 & CVE-2021-33571. (lamby)
  NOTE: 20231020: Also now vulnerable to CVE-2023-43665. (lamby)
--
python-glance-store
  NOTE: 20230525: Added by Front-Desk (lamby)
  NOTE: 20230525: NB. CVE-2023-2088 filed against python-glance-store, python-os-brick, nova and cinder.
  NOTE: 20230705: pushed a patched version to: https://salsa.debian.org/lts-team/packages/python-glance-store (jspricke)
  NOTE: 20230705: upstream patch looks fine to me but should probably be tested and released together with the other affected packages. (jspricke)
--
python-os-brick
  NOTE: 20230525: Added by Front-Desk (lamby)
  NOTE: 20230525: NB. CVE-2023-2088 filed against python-glance-store, python-os-brick, nova and cinder.
--
rails
  NOTE: 20220909: Re-added due to regression (abhijith)
  NOTE: 20220909: Regression on 2:5.2.2.1+dfsg-1+deb10u4 (abhijith)
  NOTE: 20220909: Two issues https://lists.debian.org/debian-lts/2022/09/msg00014.html (abhijith)
  NOTE: 20220909: https://lists.debian.org/debian-lts/2022/09/msg00004.html (abhijith)
  NOTE: 20220909: upstream report https://github.com/rails/rails/issues/45590 (abhijith)
  NOTE: 20220915: 2:5.2.2.1+dfsg-1+deb10u5 uploaded without the regression causing patch (abhijith)
  NOTE: 20220915: Utkarsh prepared a patch and is on testing (abhijith)
  NOTE: 20221003: https://github.com/rails/rails/issues/45590#issuecomment-1249123907 (abhijith)
  NOTE: 20221024: Delay upload, see above comment, users have done workaround. Not a good idea
  NOTE: 20221024: to break thrice in less than 2 month.
  NOTE: 20230131: Utkarsh to start a thread with sec+ruby team with the possible path forward. (utkarsh)
  NOTE: 20230828: want to rollout ruby-rack first. (utkarsh)
--
ring
  NOTE: 20230903: Added by Front-Desk (gladk)
  NOTE: 20230928: will be likely hard to fix see https://lists.debian.org/debian-lts/2023/09/msg00035.html (rouca)
--
salt
  NOTE: 20220814: Added by Front-Desk (gladk)
  NOTE: 20220814: I am not sure, whether it is possible to fix issues
  NOTE: 20220814: without backporting a newer version. (Anton)
  NOTE: 20230720: Backport to at least 3002.9 in order to fix protocol flaws between client/server
  NOTE: 20230720: Users will need need both update client and server synchronously (flag day).
  NOTE: 20230720: Unfortunatly upgrading will need to update some configuration file
  NOTE: 20230720: https://docs.saltproject.io/en/master/topics/releases/2019.2.0.html#non-backward-compatible-change-to-yaml-renderer
  NOTE: 20230720: They are also some minor change here:
  NOTE: 20230720: https://docs.saltproject.io/en/master/topics/releases/3002.html#execution-module-changes
  NOTE: 20230720: Last but not least salt is not present in stable/testing (rouca)
  NOTE: 20230928: Backported 3002.9 first non affected by crypto flaw version
  NOTE: 20230928: will need python3-saltfactories >= 0.907 (that need python3-setuptools (>= 50.3.2),  python3-setuptools-scm (>= 3.4) to be investigated)
  NOTE: 20230928: will need python3-attr (>= 19.1) may from buster-backport ? or vendored ?
  NOTE: 20230928: see https://lists.debian.org/debian-lts/2023/09/msg00033.html
--
samba
  NOTE: 20230918: Added by Front-Desk (apo)
--
squid (Markus Koschany)
  NOTE: 20231102: Added by Front-Desk (lamby)
  NOTE: 20231218: Investigating new CVE. (apo)
--
suricata (Adrian Bunk)
  NOTE: 20230620: Added by Front-Desk (Beuc)
  NOTE: 20230620: 15+ CVEs marked no-dsa; since the package is supported, with last LTS update in Jessie,
  NOTE: 20230620: I'd suggest reviewing the CVEs, precise the triage (postponed/ignored),
  NOTE: 20230620: and possibly issue a DSA with a few CVEs that were fixed in later dists (Beuc/front-desk)
  NOTE: 20230714: Still reviewing+testing CVEs. (bunk)
  NOTE: 20230731: Still reviewing+testing CVEs. (bunk)
  NOTE: 20231016: Still reviewing+testing CVEs. (bunk)
  NOTE: 20231120: DLA coming soon. (bunk)
--
tinymce
  NOTE: 20231123: Added by Front-Desk (ola)
  NOTE: 20231216: Someone with more XSS experience needed to assess the
  NOTE: 20231216: severity of CVE-2023-48219.  Also not clear to me that
  NOTE: 20231216: upstream's patch is backportable, as the code has changed a
  NOTE: 20231216: lot.  (spwhitton)
--
tomcat9
  NOTE: 20231129: Added by Front-Desk (Beuc)
  NOTE: 20131217: I have made a fix, tests are ok but due to high popcon prefer a review by apo (rouca)
--
varnish (Abhijith PA)
  NOTE: 20231117: Added by Front-Desk (apo)
  NOTE: 20231204: Working on pre commits for CVE-2023-44487, https://github.com/varnishcache/varnish-cache/pull/4004
  NOTE: 20231219: Continuing work
--
wireshark (Adrian Bunk)
  NOTE: 20231118: Added by Front-Desk (apo)
  NOTE: 20231204: DLA pending (bunk)
  NOTE: 20231218: Debugging a problem with the update. (bunk)
--
zabbix
  NOTE: 20231015: Added by Front-Desk (ta)
--
zfs-linux (utkarsh)
  NOTE: 20231127: Added by Front-Desk (Beuc)
--
