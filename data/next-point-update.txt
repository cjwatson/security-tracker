CVE-2023-3153
	[bookworm] - ovn 23.03.1-1~deb12u1
CVE-2023-43040
	[bookworm] - ceph 16.2.11+ds-2+deb12u1
CVE-2023-40481
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-31102
	[bookworm] - 7zip 22.01+dfsg-8+deb12u1
CVE-2023-3724
	[bookworm] - wolfssl 5.5.4-2+deb12u1
CVE-2023-39350
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39351
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39352
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39353
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39354
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-39356
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40181
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40186
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40188
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40567
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40569
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-40589
	[bookworm] - freerdp2 2.11.2+dfsg1-1~deb12u1
CVE-2023-4039
	[bookworm] - gcc-12 12.2.0-14+deb12u1
CVE-2023-34410
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-37369
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-38197
	[bookworm] - qtbase-opensource-src 5.15.8+dfsg-11+deb12u1
CVE-2023-49208
	[bookworm] - glewlwyd 2.7.5-3+deb12u1
CVE-2023-22084
	[bookworm] - mariadb 1:10.11.6-0+deb12u1
