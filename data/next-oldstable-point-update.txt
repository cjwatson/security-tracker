CVE-2023-32665
	[bullseye] - glib2.0 2.66.8-1+deb11u1
CVE-2023-32611
	[bullseye] - glib2.0 2.66.8-1+deb11u1
CVE-2023-29499
	[bullseye] - glib2.0 2.66.8-1+deb11u1
CVE-2023-5157
	[bullseye] - galera-4 26.4.14-0+deb11u1
CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-29967
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2023-49208
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2021-24119
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2021-44732
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2022-32096
	[bullseye] - rhonabwy 0.9.13-3+deb11u2
CVE-2022-2996
	[bullseye] - python-scciclient 0.8.0-2+deb11u1
CVE-2022-42961
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-39173
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-42905
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-24859
	[bullseye] - pypdf2 1.26.0-4+deb11u1
CVE-2022-48279
	[bullseye] - modsecurity-apache 2.9.3-3+deb11u2
CVE-2023-24021
	[bullseye] - modsecurity-apache 2.9.3-3+deb11u2
CVE-2023-0842
	[bullseye] - node-xml2js 0.2.8-1+deb11u1
CVE-2022-0512
	[bullseye] - node-url-parse 1.5.3-1+deb11u2
CVE-2021-3574
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-4219
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20241
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20243
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20244
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20245
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20246
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-20309
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2021-39212
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2022-1114
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2022-28463
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2022-32545
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2022-32546
	[bullseye] - imagemagick 8:6.9.11.60+dfsg-1.3+deb11u2
CVE-2023-26136
	[bullseye] - node-tough-cookie 4.0.0-2+deb11u1
CVE-2023-26132
	[bullseye] - node-dottie 2.0.2-4+deb11u1
CVE-2023-40743
	[bullseye] - axis 1.4-28+deb11u1
CVE-2023-46586
	[bullseye] - weborf 0.17-3+deb11u1
CVE-2021-33880
	[bullseye] - python-websockets 8.1-1+deb11u1
CVE-2023-46734
	[bullseye] - symfony 4.4.19+dfsg-2+deb11u4
CVE-2023-31022
	[bullseye] - nvidia-graphics-drivers 470.223.02-1
CVE-2023-45853
	[bullseye] - minizip 1.1-8+deb11u1
CVE-2023-31022
	[bullseye] - nvidia-graphics-drivers-tesla-470 470.223.02-1~deb11u1
CVE-2023-47038
	[bullseye] - perl 5.32.1-4+deb11u3
CVE-2023-27102
	[bullseye] - libde265 1.0.11-0+deb11u2
CVE-2023-27103
	[bullseye] - libde265 1.0.11-0+deb11u2
CVE-2023-43887
	[bullseye] - libde265 1.0.11-0+deb11u2
CVE-2023-47471
	[bullseye] - libde265 1.0.11-0+deb11u2
CVE-2020-22218
	[bullseye] - libssh2 1.9.0-2+deb11u1
CVE-2023-5981
	[bullseye] - gnutls28 3.7.1-5+deb11u4
CVE-2023-22084
	[bullseye] - mariadb-10.5 1:10.5.23-0+deb11u1
CVE-2022-48521
	[bullseye] - opendkim 2.11.0~beta2-4+deb11u1
